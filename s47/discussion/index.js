// The 'document' represent the whole HTML page, and the 'querySelector' targets the specific element based on its ID, class, or tag name
// ID - '#', Class = '.'
console.log(document.querySelector('#txt-first-name'))

// These are the specific selectors you can use for targeting a specific target method (ID, class, tag name)
// Note: These aren't common to use anymore as compared to 'querySelector'
// If you want to target the ID
document.getElementById('txt-first-name')
// If you want to target the Class Name
document.getElementsByClassName('span-full-name')
// If you want to target the Tag Name
document.getElementsByTagName('input')


// [SECTION] Event Listeners
const txt_first_name = document.querySelector('#txt-first-name')
// Activity
const txt_last_name = document.querySelector('#txt-last-name')

const txt_age = document.querySelector('#txt-age')
const txt_address = document.querySelector('#txt-address')

let span_full_details = document.querySelector('.span-full-details')

// The 'addEventListener' function listens for an event in a specific HTML tag. Anytime an event is triggered, it will run the function in its 2nd argument
txt_first_name.addEventListener('keyup', (event) => {
	span_full_details.innerHTML = `Hello, I am ${txt_first_name.value} ${txt_last_name.value}, ${txt_age.value} years old. I am from ${txt_address.value}`
})

txt_last_name.addEventListener('keyup', (event) => {
	span_full_details.innerHTML = `Hello, I am ${txt_first_name.value} ${txt_last_name.value}, ${txt_age.value} years old. I am from ${txt_address.value}`
})

txt_age.addEventListener('keyup', (event) => {
	span_full_details.innerHTML = `Hello, I am ${txt_first_name.value} ${txt_last_name.value}, ${txt_age.value} years old. I am from ${txt_address.value}`
})

txt_address.addEventListener('keyup', (event) => {
	span_full_details.innerHTML = `Hello, I am ${txt_first_name.value} ${txt_last_name.value}, ${txt_age.value} years old. I am from ${txt_address.value}`
})


txt_first_name.addEventListener('keyup', (event) => {
	// The 'event' object represents the actual event in the HTML element (keyup) and you can use that object's 'target' property to access the HTML element itself
	console.log(event.target)
	console.log(event.target.value)
})

txt_last_name.addEventListener('keyup', (event) => {
	// The 'event' object represents the actual event in the HTML element (keyup) and you can use that object's 'target' property to access the HTML element itself
	console.log(event.target)
	console.log(event.target.value)
})

txt_age.addEventListener('keyup', (event) => {
	// The 'event' object represents the actual event in the HTML element (keyup) and you can use that object's 'target' property to access the HTML element itself
	console.log(event.target)
	console.log(event.target.value)
})

txt_address.addEventListener('keyup', (event) => {
	// The 'event' object represents the actual event in the HTML element (keyup) and you can use that object's 'target' property to access the HTML element itself
	console.log(event.target)
	console.log(event.target.value)
})